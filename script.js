//Dom elements
const time = document.getElementById('time'),
  greeting = document.getElementById('greeting'),
  name = document.getElementById('name'),
  focus = document.getElementById('focus');

//Show time
function showTime() {
  let today = new Date(),
    hour = today.getHours(),
    min = today.getMinutes(),
    sec = today.getSeconds();

  //set AR of PM
  const amPm = hour >= 12 ? 'PM' : 'AM';

  //12 hr format
  hour = hour % 12 || 12;

  //Output time
  time.innerHTML = `${hour}<span>:</span> ${addZero(
    min
  )} <span>:</span> ${addZero(sec)}`;
  setTimeout(showTime, 1000);
}

//add zero
function addZero(n) {
  return (parseInt(n, 10) < 10 ? '0' : '') + n;
}

//set Background
function setBgGreet() {
  let today = new Date();
  hour = today.getHours();

  if (hour < 12) {
    document.body.style.backgroundColor = 'green';
    greeting.textContent = 'good morning';
  } else if (hour < 18) {
    document.body.style.backgroundColor = 'gray';
    greeting.textContent = 'good afternoon';
  } else {
    document.body.style.backgroundColor = 'orange';
    greeting.textContent = 'good evening';
    document.body.style.color = 'white';
  }
}



function getName() {
  if (localStorage.getItem('name') === null) {
    name.textContent = '[Enter Name]';
  } else {
    name.textContent = localStorage.getItem('name');
  }
}

// Set Name
function setName(e) {
  if (e.type === 'keypress') {
    // Make sure enter is pressed
    if (e.which == 13 || e.keyCode == 13) {
      localStorage.setItem('name', e.target.innerText);
      name.blur();
    }
  } else {
    localStorage.setItem('name', e.target.innerText);
  }
}

//get focus
function getFocus() {
  if (localStorage.getItem('focus') === null) {
    focus.textContent = '[Enter focus]';
  } else {
    focus.textContent = localStorage.getItem('focus');
  }
}

function setFocus(e) {
  if (e.type === 'keypress') {
    // Make sure enter is pressed
    if (e.which == 13 || e.keyCode == 13) {
      localStorage.setItem('focus', e.target.innerText);
      focus.blur();
    }
  } else {
    localStorage.setItem('focus', e.target.innerText);
  }
}


name.addEventListener('keypress', setName);
name.addEventListener('blur', setName);
focus.addEventListener('keypress', setFocus);
focus.addEventListener('blur', setFocus);


//run
showTime();
setBgGreet();
getFocus();
getName();

